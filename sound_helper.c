#include <pic.h>
#include "sound_helper.h"
#include "utils.h"
#include "data_type.h"

void playTone(uchar Tone) {
	 RC0 =~RC0;
	 //NOTE 延时表决定了每个音符的频率
     for(uint k=0;k< Tone / 2;k++);
}

void playMusic(uchar SONG_LONG[], uchar SONG_TONE[]) {
	uint i=0,j;
	while(SONG_LONG[i]!=0||SONG_TONE[i]!=0){
		//播放各个音符，SONG_LONG 为拍子长度
    	for(j=0;j<SONG_LONG[i] * 10;j++) {
    		playTone(SONG_TONE[i]);
		}
    DelayMS(5);
    i++;
	}
}