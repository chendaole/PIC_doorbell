#ifndef _SOUND_HELPR_H_
#define _SOUND_HELPR_H_

#include "data_type.h"

void playTone(uchar tone);
void playMusic(uchar SONG_LONG[], uchar SONG_TONE[]);

#endif 